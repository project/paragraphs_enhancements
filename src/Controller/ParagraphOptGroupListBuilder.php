<?php

namespace Drupal\paragraphs_enhancements\Controller;

/**
 * Contains Drupal\paragraphs_enhancements\Controller\ParagraphOptGroupListBuilder.
 */

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Builds the listing page for our Paragraph OptGroup config entity.
 *
 * Inherit from DraggableListBuilder so we can re-order the optgroups without
 * having to edit each entity.
 */
class ParagraphOptGroupListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  protected $limit = FALSE;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'paragraphs_enhancements_paragraph_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Group name');
    $header['machine_name'] = $this->t('Machine name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['machine_name']['#markup'] = $entity->id();

    return $row + parent::buildRow($entity);
  }

}
