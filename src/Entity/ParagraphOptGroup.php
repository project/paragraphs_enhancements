<?php

namespace Drupal\paragraphs_enhancements\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\paragraphs_enhancements\ParagraphOptGroupInterface;

/**
 * Defines the Paragraph OptGroup entity.
 *
 * @ConfigEntityType(
 *   id = "paragraph_optgroup",
 *   label = @Translation("Paragraph OptGroup"),
 *   handlers = {
 *     "list_builder" = "Drupal\paragraphs_enhancements\Controller\ParagraphOptGroupListBuilder",
 *     "form" = {
 *       "add" = "Drupal\paragraphs_enhancements\Form\ParagraphOptGroupForm",
 *       "edit" = "Drupal\paragraphs_enhancements\Form\ParagraphOptGroupForm",
 *       "delete" = "Drupal\paragraphs_enhancements\Form\ParagraphOptGroupDeleteForm",
 *     }
 *   },
 *   config_prefix = "paragraph_optgroup",
 *   admin_permission = "administer paragraphs settings",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "weight" = "weight"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "weight",
 *     "paragraphs"
 *   },
 *   links = {
 *     "collection" = "/admin/config/paragraph_settings_form",
 *     "edit-form" = "/admin/config/paragraph_settings_form/{paragraph_optgroup}/edit",
 *     "delete-form" = "/admin/config/paragraph_settings_form/{paragraph_optgroup}/delete",
 *   }
 * )
 */
class ParagraphOptGroup extends ConfigEntityBase implements ParagraphOptGroupInterface {

  /**
   * The Paragraph OptGroup ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Paragraph OptGroup label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Paragraph OptGroup weight.
   *
   * @var int
   */
  protected $weight;

  /**
   * The Paragraph OptGroup paragraphs.
   *
   * @var [string]
   */
  protected $paragraphs;

}
