<?php

namespace Drupal\paragraphs_enhancements;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for the Paragraph OptGroup entity.
 */
interface ParagraphOptGroupInterface extends ConfigEntityInterface {
}
