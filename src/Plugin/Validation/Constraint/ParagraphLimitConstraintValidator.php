<?php

namespace Drupal\paragraphs_enhancements\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the ParagraphLimit constraint.
 */
class ParagraphLimitConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a ValidReferenceConstraintValidator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {

    $item = $items->first();
    if (!$item) {
      return;
    }

    $paragraphs = $this->filterParagraphs($items->referencedEntities());

    if (!empty($paragraphs)) {
      // A paragraph should not be added to a field more than its limit allows.
      foreach ($paragraphs as $paragraph) {
        $count = count($paragraph['items']);

        // Get the field definition.
        $field_definition = $items->getFieldDefinition();

        // Get the form display. Needed to get the field widget.
        $form_display = $this->entityTypeManager
          ->getStorage('entity_form_display')
          ->load(
            $field_definition->getTargetEntityTypeId() .
            '.' .
            $field_definition->getTargetBundle() .
            '.default'
          );

        // Get the field widget.
        $field_widget = $form_display->getComponent($field_definition->getName());

        xdebug_break();
        if ($count > (int) $paragraph['limit']) {
          $this->context->addViolation($constraint->message, [
            '%limit' => $paragraph['limit'],
            '%plural_title' => $field_widget['settings']['title_plural'],
            '%paragraph_type' => $paragraph['label'],
          ]);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function filterParagraphs($paragraphs) {
    $limited_paragraphs = [];

    foreach ($paragraphs as $paragraph) {
      $paragraph_type = $paragraph->getParagraphType();

      $limit_plugin = $paragraph_type->getBehaviorPlugin('limit')->getConfiguration();
      if (isset($limit_plugin['enabled']) && $limit_plugin['enabled']) {
        $limited_paragraphs[$paragraph_type->id()]['label'] = $paragraph_type->label();
        $limited_paragraphs[$paragraph_type->id()]['limit'] = $limit_plugin['limit'];
        $limited_paragraphs[$paragraph_type->id()]['items'][] = $paragraph;
      }
    }
    return $limited_paragraphs;
  }

}
