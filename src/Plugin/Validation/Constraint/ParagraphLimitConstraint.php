<?php

namespace Drupal\paragraphs_enhancements\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a limit is set on the paragraph.
 *
 * @Constraint(
 *   id = "ParagraphLimit",
 *   label = @Translation("Paragraph Limit", context = "Validation"),
 * )
 */
class ParagraphLimitConstraint extends Constraint {

  /**
   * The message will be shown if more than one paragraph of this type is added.
   *
   * @var string
   */
  public $message = 'No more than %limit "%plural_title" of type "%paragraph_type" is allowed.';

}
