<?php

namespace Drupal\paragraphs_enhancements\Plugin\paragraphs\Behavior;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * Provides a Paragraphs Limit plugin.
 *
 * @ParagraphsBehavior(
 *   id = "limit",
 *   label = @Translation("Limit number of items"),
 *   description = @Translation("Allow a maximum number of paragraphs of this type to be added."),
 *   weight = 0
 * )
 */
class ParagraphsLimitPlugin extends ParagraphsBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    // No form required.
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraphs_entity, EntityViewDisplayInterface $display, $view_mode) {
    // We don't need to render anything.
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Limit content'),
      '#description' => $this->t('Limit how many of this type of paragraphs can be added. 0 is unlimited.'),
      '#default_value' => !empty($this->configuration['limit']) ? $this->configuration['limit'] : 0,
      '#min' => 0,
      '#step' => 1,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['limit'] = $form_state->getValue('limit');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'limit' => 0,
    ];
  }

}
