<?php

namespace Drupal\paragraphs_enhancements\Plugin\Field\FieldWidget;

use Drupal\paragraphs\Plugin\Field\FieldWidget\ParagraphsWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'paragraphs_enhancements' paragraphs widget.
 *
 * @FieldWidget(
 *   id = "paragraphs_enhancements",
 *   label = @Translation("Paragraphs Enhanced"),
 *   description = @Translation("Extends Paragraphs EXPERIMENTAL to add  optgroup functionality."),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class ParagraphsEnhancementsWidget extends ParagraphsWidget {

  /**
   * EntityType manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    $pluginId,
    $pluginDefinition,
    array $configuration,
    ContainerInterface $container
  ) {
    $instance = parent::create(
      $pluginId,
      $pluginDefinition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings']
    );
    $instance->setEntityTypeManager($container->get('entity_type.manager'));
    return $instance;
  }

  /**
   * Retrieves the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  public function getEntityTypeManager() {
    return $this->entityTypeManager ?: \Drupal::entityTypeManager();
  }

  /**
   * Sets the EntityType Manager.
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    return $this;
  }

  /**
   * Returns the available paragraphs type.
   *
   * @return array
   *   Available paragraphs types.
   */
  protected function getAccessibleOptions() {
    // This function is called twice for some reason. Do not remove!
    // @TODO: investigate why this is and what we can do about it.
    if (!empty($this->accessOptions)) {
      return $this->accessOptions;
    }

    // Get accessible options from parent widget.
    $this->accessOptions = parent::getAccessibleOptions();

    // If the widget is not using 'Select' for 'Add mode', do nothing.
    if ($this->getSetting('add_mode') != 'select') {
      return $this->accessOptions;
    }

    // Get general optgroup config.
    $paragraph_optgroups = $this->getEntityTypeManager()
      ->getStorage('paragraph_optgroup')
      ->getQuery()
      ->sort('weight', 'ASC')
      ->execute();

    // Get this field's accessible paragraphs.
    $unordered_paragraphs = $this->accessOptions;

    $ordered_groups = [];
    foreach ($paragraph_optgroups as $optgroup) {
      $optgroup = $this->getEntityTypeManager()->getStorage('paragraph_optgroup')->load($optgroup);

      foreach ($unordered_paragraphs as $machine_name => $label) {
        if (in_array($machine_name, $optgroup->get('paragraphs'))) {
          $ordered_groups[$optgroup->label()][$machine_name] = $label;
          unset($unordered_paragraphs[$machine_name]);
        }
      }
    }

    // Sort option values if alphabetical sort if configured.
    if (!empty($this->getSetting('alphabetical_sort'))) {
      // Sort the groups one by one.
      // We still want to keep the groups themselves ordered as per the config.
      foreach ($ordered_groups as $group_name => $group) {
        natcasesort($ordered_groups[$group_name]);
      }
      // Sort the non grouped options.
      natcasesort($unordered_paragraphs);
    }

    // Merge grouped and non grouped options, grouped options first.
    return $this->accessOptions = $ordered_groups + $unordered_paragraphs;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['alphabetical_sort'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable alphabetical sort of options.'),
      '#description' => $this->t('This will sort the list of paragraphs alphabetically (ascending).'),
      '#default_value' => $this->getSetting('alphabetical_sort'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $value = (empty($this->getSetting('alphabetical_sort'))) ? 'No' : 'Yes';
    $summary[] = $this->t('Alphabetical sort: @alpha_sort', ['@alpha_sort' => $value]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();
    $settings['alphabetical_sort'] = FALSE;

    return $settings;
  }

}
