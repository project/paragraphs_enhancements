<?php

namespace Drupal\paragraphs_enhancements\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The ParagraphOptGroup configuration form.
 */
class ParagraphOptGroupForm extends EntityForm implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Create an instance of ContentModerationConfigureForm.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityTypeBundleInfoInterface $entityTypeBundleInfo) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $paragraph_optgroup = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $paragraph_optgroup->label(),
      '#description' => $this->t("Label for the OptGroup."),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $paragraph_optgroup->id(),
      '#machine_name' => [
        'exists' => '\Drupal\paragraphs_enhancements\Entity\ParagraphOptGroup::load',
      ],
      '#disabled' => !$paragraph_optgroup->isNew(),
    ];

    $form['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight'),
      '#default_value' => $paragraph_optgroup->get('weight'),
    ];

    $header = [
      'type' => $this->t('Items'),
      'operations' => $this->t('Operations'),
    ];
    $form['paragraph_types_container'] = [
      '#type' => 'details',
      '#title' => $this->t('This paragraph group applies to:'),
    ];
    $form['paragraph_types_container']['paragraphs'] = [
      '#type' => 'table',
      '#header' => $header,
      '#empty' => $this->t('There are no paragraph types.'),
    ];

    // Get all existing paragraph optgroups.
    $paragraph_optgroups = $this->entityTypeManager->getStorage('paragraph_optgroup')->loadMultiple();

    // Build out the options list.
    // A paragraph to be added to more than one optgroup.
    $options = $defaults = [];
    $selected_paragraphs = $paragraph_optgroup->get('paragraphs');
    foreach ($this->entityTypeBundleInfo->getBundleInfo('paragraph') as $bundle_id => $bundle) {
      foreach ($paragraph_optgroups as $optgroup) {
        if ($optgroup->id() != $paragraph_optgroup->id() && in_array($bundle_id, $optgroup->get('paragraphs'))) {
          // Exit both foreach loops.
          continue 2;
        }
      }
      $options[$bundle_id] = [
        'title' => ['data' => ['#title' => $bundle['label']]],
        'type' => $bundle['label'],
      ];

      if (!empty($selected_paragraphs)) {
        $defaults[$bundle_id] = in_array($bundle_id, $selected_paragraphs);
        // Add the enabled flag so we can sort this array later.
        $options[$bundle_id]['enabled'] = $defaults[$bundle_id];
      }
    }

    if (!empty($options)) {
      $bundles_header = $this->t('All @entity_type types', ['@entity_type' => $this->t('paragraph')]);

      // Sort the paragraphs.
      // Enabled paragraphs at the top, then sorted alphabetically.
      $names = array_column($options, 'type');
      $enabled = array_column($options, 'enabled');
      if (!empty($enabled)) {
        array_multisort($enabled, SORT_DESC, $names, SORT_ASC, SORT_NATURAL | SORT_FLAG_CASE, $options);
      }
      else {
        array_multisort($names, SORT_ASC, SORT_NATURAL | SORT_FLAG_CASE, $options);
      }

      // Build the table.
      $form['paragraph_types_container']['paragraphs'] = [
        '#type' => 'tableselect',
        '#header' => [
          'type' => $bundles_header,
        ],
        '#options' => $options,
        '#default_value' => $defaults,
        '#attributes' => ['class' => ['no-highlight']],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $paragraph_optgroup = $this->entity;

    $selected_paragraphs = [];

    foreach ($form_state->getValue('paragraphs') as $bundle_id => $checked) {
      if ($checked) {
        $selected_paragraphs[] = $bundle_id;
      }
    }

    $paragraph_optgroup->set('paragraphs', $selected_paragraphs);
    $status = $paragraph_optgroup->save();

    if ($status) {
      $this->messenger()->addMessage($this->t('Saved the %label paragraph optgroup.', [
        '%label' => $paragraph_optgroup->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('An error occured. The %label paragraph optgroup was not saved.', [
        '%label' => $paragraph_optgroup->label(),
      ]));
    }

    $form_state->setRedirect('entity.paragraph_optgroup.collection');
  }

}
