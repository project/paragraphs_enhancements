# Paragraphs Enhancements README
This module provides editorial and site-building enhancements for paragraphs.

## Installation / Configuration
Install as per any other Drupal Module. https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules

## Functionality
### Paragraphs OptGroups
- Allows site-builders to group paragraphs together so they are easier to find
and use by content authors.
- A new widget ('Paragraphs Enhanced') is provided to help make use of this
functionality.
- To use first create a few paragraph types in Admin > Structure > Paragraphs.
- In Admin > Config > Paragraph optgroups add the optgroups you need and groups
your paragraphs together.
e.g. 'Layout' for your layout related paragraphs, and 'Content' for your content
related paragraphs.
- On the content type where you will use these paragraphs, select the
'Paragraphs Enhanced' widget for the field form display.
- In the configuration for the widget use 'Select' as the 'Add mode' for the
field.

### Paragraphs Enhancements Widget
- Inherits from Paragraphs Experimental Widget to preserve functionality.
- Allows paragraphs to be grouped in OptGroups. Widget *must* be set to use
'Select' list.
- Adds option for paragraphs to be sorted alphabetically.

### ParagraphLimit Behaviour
- Adds an optional behavior to paragraphs.
- When enabled, limits how many times a paragraph can be added to a field.

## Roadmap
- What enhancements can we do for the other add modes for the widget?
- Add tests.
- Should the config menu link for this module be placed as a tab under
'Paragraphs Settings'?
- What other enhancements could we add?
